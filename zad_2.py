
"""
Created on Tue Dec  1 21:43:46 2015
#Napravite jedan vektor proizvoljne duzine koji sadrzi slucajno generirane 
#cjelobrojne vrijednosti 0 ili 1. Neka 1 oznacava musku osobu, a 0 zensku osobu.
# Napravite drugi vektor koji sadrzi visine osoba koje se dobiju uzorkovanjem
#odgovarajuce distribucije. U slucaju muskih osoba neka je to Gaussova 
#distribucija sa srednjom vrijednoscu 180 cm i standardnom devijacijom 7 cm, 
#dok je u slucaju zenskih osoba to Gaussova distribucija sa srednjom 
#vrijednoscu 167 cm i standardnom devijacijom 7 cm. Prikazite podatke te ih 
#obojite plavom (1) odnosno crvenom bojom (0). Napisite funkciju koja racuna 
#srednju vrijednost visine za muskarce odnosno zene (probajte izbjeci for 
#petlju pomocu funkcije np.dot). Prikazite i dobivene srednje vrijednosti na 
#grafu.
@author: bec
"""

import numpy as np
import matplotlib.pyplot as plt
#import os, stat 
#from numpy import *
from scipy.stats import norm

i=j=1

n=raw_input('Broj zena i muskaraca:')
try :
    n=int(n)
   


    #print vek
    vek= np.random.randint(2,size=n) 
    d = dict.fromkeys(vek, 0)               #stavljanje u dictionary, kako bi znali tocan broj muskih i tocan broj zenskih
    for h in vek:                      #odredjivanje koliko tocno ima muskaraca a koliko zena u dict
        if h  in d:              
           d[h] = d[h] + 1 
    #print d
    lm=d.get(1)        #broj muskaraca i zena u dict
    lz=d.get(0)
    #print lm
    #print lz
    
    vis_lm=np.zeros(lm)     #definiranje np.arraa duljine koliko ima muskaraca, odnosno zena
    #print vis_lm
    vis_lz=np.zeros(lz)
    
    for i in range(0,lm):
              #gaus, u ovisnosi za muske i zenske
            vis_lm[i]=np.random.normal(180,7)
            
    for j in range(0,lz):
            vis_lz[j]=np.random.normal(167,7)
            
    #print 'muskarci',vis_lm
    #print 'zene',vis_lz
    
    vis_lms= sorted(vis_lm)  #sortiranje arraya
    vis_lzs= sorted(vis_lz)
    
    zpm=norm.pdf(vis_lms, np.mean(vis_lms), np.std(vis_lms))  #priprema Za Plotanje 
    zpz=norm.pdf(vis_lzs, np.mean(vis_lzs), np.std(vis_lzs))
     
    
    
    srv_m=sum(vis_lm)/len(vis_lm)
    srv_mA=np.dot(srv_m,np.ones(len(vis_lm))) #mnozenje srv_m (skalar) sa array koji sve elemente ima jedan i duljine je vektora vis_lm,kako bi se moglo plotat
    
    srv_z=sum(vis_lz)/len(vis_lz)
    srv_zA=np.dot(srv_z,np.ones(len(vis_lz)))
    
    
    plt.plot(vis_lms,zpm,'bo')
    plt.plot(vis_lzs,zpz,'ro')
    plt.plot(srv_mA,zpm,'b')
    plt.plot(srv_zA,zpz,'r')
    print 'srednja vrijednost muskarci : ',srv_m
    print 'srednja vrijednost zene : ',srv_z

except :
    print 'molim unosite samo cijele brojeve'



