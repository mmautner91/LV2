# -*- coding: utf-8 -*-
'''
Simulirajte 100 bacanja igrace kocke (kocka s brojevima 1 do 6).
 Pomocu histograma prikažite rezultat ovih bacanja.
'''

import numpy as np
#import plotly.graph_objs as go
import matplotlib.pyplot as plt
import random 
import pylab

n=np.random.choice([1,2,3,4,5,6],100)
print n
bins = numpy.linspace(0, 10, 50)
plt.hist(n,bins)
