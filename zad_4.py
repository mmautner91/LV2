# -*- coding: utf-8 -*-
'''
Simulirajte bacanje igraće kocke 30 puta.
Izracunajte srednju vrijednost rezultata i standardnu devijaciju. Pokus
ponovite 1000 puta. Prikazite razdiobu dobivenih srednjih vrijednosti.
sto primjecujete? Kolika je srednja vrijednost i
standardna devijacija dobivene razdiobe? 
sto se dogadja s ovom razdiobom ako pokus ponovite 10000 puta?

'''

import numpy as np
import matplotlib.pyplot as plt
i=0
br=1000
sr_v=st_dev=np.zeros(br)

for i in range (0,br):
    n=np.random.choice([1,2,3,4,5,6],30)
    sr_v[i]=np.mean(n)  #racunanje srednje vrijednosti
    st_dev[i]=np.std(n)   #izracun standardne devijacije
    print sr_v[i]
    
bins = numpy.linspace(-10, 10, br*10)   #oblikovanje histograma


fig1 = plt.figure()     #histogrami
plt.hist(sr_v,bins,color='green')
plt.title('Histogram srednjih vrijednosti')


fig2 = plt.figure()
plt.hist(st_dev,bins,color='red')
plt.title('Histogram standardnih devijacija')


#plt.show        
